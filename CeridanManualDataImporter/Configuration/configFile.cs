﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CeridianManualDataImporter.Configuration
{
    class configFile
    {
        private static readonly NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        // Settings: 
        public string getCalabrioURL()
        {
            return (getValue(true, "CalabrioURL"));
        }
        public string getCalabrioUsername()
        {
            return (getValue(true, "CalabrioApiUsername"));
        }
        public string getCalabrioPassword()
        {
            return (getValue(true, "CalabrioApiPassword"));
        }
        public int getCalabrioTenantID()
        {
            return (Convert.ToInt32(getValue(true, "CalabrioApiTenantId")));
        }

        private void ApplySetting(string key, string value)
        {
            // Modify a configuration value.
            var configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var settings = configFile.AppSettings.Settings;
            if (settings[key] == null)
            {
                settings.Add(key, value);
            }
            else
            {
                settings[key].Value = value;
            }
            configFile.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection(configFile.AppSettings.SectionInformation.Name);
        }
        private string getValue(bool required, string appSetting, string defaultValue = "", bool reloadSettings = false)
        {
            // Required: Application cannot run without this value.
            // Default value: We can set a default if the user did not, however if required is set a default will not be set.
            // AppSetting: The value in the config to search for. 
            // NOTE this method will only log the option its looking for not the output, as passwords can be an "output" 

            try
            {
                if (reloadSettings == true) // save runtime only do this if reloadSettings is set. 
                {
                    ConfigurationManager.RefreshSection("appSettings"); // Reload settings each time this is called! 
                }

                string configValue = ConfigurationManager.AppSettings[appSetting];

                // Validate that configValue has data. 
                if (string.IsNullOrEmpty(configValue))
                {
                    // Check if using the default is OK:
                    if (required == true)
                    {
                        logger.Fatal("Unable to determine value of: " + appSetting + " This setting is REQUIRED, exiting application.");
                        Environment.Exit(0); // Fatal error exit application, we need this to work.
                        return configValue;
                    }
                    else // Using Default = OK set value as such and continue
                    {
                        logger.Error("Config value not set for: " + appSetting + " Setting to Default Value: " + defaultValue);
                        return defaultValue;
                    }
                }
                else
                {
                    return configValue;
                }
            }
            catch (Exception ex)
            {
                ConfigurationManager.RefreshSection("appSettings"); // Reload settings upon failure, next run may need a refresh.
                if (required == false)
                {
                    logger.Error(ex.Message + " Config value not set for: " + appSetting + " Setting to Default Value: " + defaultValue);
                    return defaultValue;
                }
                if (required == true)
                {
                    logger.Fatal(ex.Message + " Unable to determine value of: " + appSetting);
                    Environment.Exit(0); // Fatal error exit application, we need this to work.
                    return null; // Make the compiler happy. 
                }
                logger.Error(ex.Message, "Unknown Error");
                return defaultValue;
            }
        }

        private Boolean ToBoolean(string value)
        {
            switch (value.ToLower())
            {
                case "true":
                    return true;
                case "t":
                    return true;
                case "1":
                    return true;
                case "0":
                    return false;
                case "false":
                    return false;
                case "f":
                    return false;
                default:
                    throw new InvalidCastException("You can't cast that value to a bool!");
            }
        }


    }
}
