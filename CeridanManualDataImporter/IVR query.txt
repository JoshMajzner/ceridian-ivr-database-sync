DECLARE @startDate datetime; -- Should be 'Yesterday'
DECLARE @endDate datetime; -- Should be 'Today'

set @startDate = dateadd(day,datediff(day,1,GETDATE()),0);
set @endDate = dateadd(day,datediff(day,0,GETDATE()),0);

print @startDate 
print @endDate

select * from IVR_DB_PROD.dbo.IVR_CDR_DATA 
where DATETIME between @startDate and @endDate order by DATETIME asc