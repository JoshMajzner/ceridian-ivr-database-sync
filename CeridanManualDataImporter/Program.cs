﻿
using System;
using System.Collections.Generic;

namespace CeridianManualDataImporter
{
    class Program
    {
        private static readonly NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        static void Main(string[] args)
        {
            Configuration.configFile config = new Configuration.configFile();
            Calabrio.API.Authentication.Authorization auth = new Calabrio.API.Authentication.Authorization(); // Calabrio Authentication
            Calabrio.API.RecordingControls.Metadata metadata = new Calabrio.API.RecordingControls.Metadata(); // Calabrio: Modify contact metadata.
            Calabrio.API.GenericTextImport.TextImport text = new Calabrio.API.GenericTextImport.TextImport(); // Calabrio Text import
            Calabrio.API.Contact.search search = new Calabrio.API.Contact.search(); // Calabrio Contact Search

            IVR.Database.query IvrQuery = new IVR.Database.query();



            // Pull IVR database for all IVR records and return a list.
            List<IVR.Database.ivrRecord> yesterdaysIvrRecords = IvrQuery.getIvrData();

            // For each record in the list, pull the calabrio API and search for any contact that the UCID matches:
            foreach (var ivrRecord in yesterdaysIvrRecords)
            {
                
            }






            //IvrQuery.connectMssql();
            //IvrQuery.CloseMssqlConnection();

            Console.ReadKey();

        }
    }


     class testMethods
    {
        private static readonly NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private Calabrio.API.Authentication.Authorization auth = new Calabrio.API.Authentication.Authorization(); // Calabrio Authentication
        private Configuration.configFile config = new Configuration.configFile();

        public testMethods()
        {
            auth.Authenticate(config.getCalabrioURL(), config.getCalabrioUsername(), config.getCalabrioPassword(), config.getCalabrioTenantID());
        }

        public Calabrio.API.Contact.contact contactSearchByID(string contactID)
        {
            Calabrio.API.Contact.search search = new Calabrio.API.Contact.search();
            return(search.getContactInfoById(auth.ac, config.getCalabrioURL(), contactID));
        }
        public List<Calabrio.API.Contact.contact> contactSearchByAssociatedContactID(string associatedID)
        {
            Calabrio.API.Contact.search search = new Calabrio.API.Contact.search();
            return (search.getContactsByAssocCallId(auth.ac, config.getCalabrioURL(), associatedID));
        }

        public void updateContactsMetadata(string contactID, string key, string value)
        {
            Calabrio.API.RecordingControls.Metadata metadata = new Calabrio.API.RecordingControls.Metadata(); // Calabrio: Modify contact metadata.
            Dictionary<string, string> metadataTest = new Dictionary<string, string>();
            metadataTest.Add(key, value);
            metadata.PostMetadataForContactID(auth.ac, config.getCalabrioURL(), contactID, metadataTest);
        }

        public void testTextImport()
        {
            Calabrio.API.GenericTextImport.TextImport textImport = new Calabrio.API.GenericTextImport.TextImport();
            string testData = textImport.generateTestData();
            textImport.submitPost(config.getCalabrioURL(), testData, auth.ac);
        }

        public void testContactDelete(string contactID)
        {
            Calabrio.API.Contact.delete delete = new Calabrio.API.Contact.delete();
            bool deleteSuccess = delete.deleteContact(auth.ac, config.getCalabrioURL(), contactID);
            if (deleteSuccess == true)
            {
                logger.Info("Contact delete success");
            }
            else
            {
                logger.Error("Contact delete failed");
            }
        }
    }

}
