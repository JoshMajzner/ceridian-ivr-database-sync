﻿using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace CeridianManualDataImporter.Calabrio.API.Authentication
{
    class Authorization
    {
        private static readonly NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        public struct AuthenticationCookies
        {
            public string jSessionId;
            public string hazelCastSessionId;
        }
        public AuthenticationCookies ac = new AuthenticationCookies();
        public Helpers.headers header = new Helpers.headers();


        // Auth Token Data: 

        /// <summary>
        /// Will try authentication, If the response comes back positive you can safely extract the authentication cookie for other requests. 
        /// </summary>
        /// <returns>
        /// Will return TRUE if API auth = successful
        /// Will return FALSE if API auth = fail
        /// </returns>
        public bool Authenticate(string calabrioURI, string username, string password, int tenantId)
        {
            sendAuthentication(calabrioURI, username, password, tenantId);
            return (authCookieNotNull(ac));
        }

        private bool authCookieNotNull(AuthenticationCookies ac)
        {
            // This method will check that the authentication cookie has data. 
            if (ac.hazelCastSessionId == null || ac.jSessionId == null)
            {
                logger.Error("Authorization Cookie returned null, API not authenticated");

                // Further Diagnostics: 
                if (ac.hazelCastSessionId == null)
                {
                    logger.Trace("hazelCastSessionId: NULL");
                }
                if (ac.jSessionId == null)
                {
                    logger.Trace("jSessionId: NULL");
                }
                return false;
            }
            else
            {
                logger.Trace("Authorization cookie has data");
                return true;
            }
        }

        private AuthenticationCookies sendAuthentication(string calabrioURI, string username, string password, int tenantId)
        {
            ServicePointManager.ServerCertificateValidationCallback +=
               (sender, cert, chain, sslPolicyErrors) => true;
            string jsonBody = GenAuthorizationJsonBody(username, password, tenantId);
            string json = JsonConvert.SerializeObject(jsonBody);

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(calabrioURI);
                client.DefaultRequestHeaders
                    .Accept
                    .Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "api/rest/authorize");
                request.Content = new StringContent(jsonBody,
                    Encoding.UTF8,
                    "application/json");
                try
                {
                    var response = client.SendAsync(request).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        logger.Info("Calabrio Request ID: " + header.getRequestID(response.Headers) + " -- Calabrio authentication success");
                        ac = parseHeaderForCookies(response.Headers);
                        logger.Trace("API authorization successful: " + ac.jSessionId + "  " + ac.hazelCastSessionId);
                    }
                    else
                    {
                        logger.Info("Calabrio Request ID: " + header.getRequestID(response.Headers) + " -- Calabrio authentication failed");
                        logger.Error("API server rejected login request: " + response.StatusCode + " - " + response.ReasonPhrase);
                        logger.Trace("Reason Phrase: " + response.ReasonPhrase);
                        header.getRequestID(response.Headers);
                    }
                    return ac;
                }
                catch (Exception ex)
                {
                    logger.Error("Unable to authenticate. " + ex.Message);
                    return ac;
                }
               

           
            }
        }
        private AuthenticationCookies parseHeaderForCookies(HttpResponseHeaders hrh)
        {
            AuthenticationCookies ac = new AuthenticationCookies();
            foreach (var header in hrh)
            {
                if (header.Key == "Set-Cookie")
                {
                    foreach (var cookie in header.Value)
                    {
                        if (cookie.Contains("JSESSIONID"))
                        {
                            ac.jSessionId = cookie.Substring(0, cookie.IndexOf(";"));
                            logger.Trace("jSessionID=" + ac.jSessionId);
                        }
                        else if (cookie.Contains("hazelcast"))
                        {
                            ac.hazelCastSessionId = cookie.Substring(0, cookie.IndexOf(";"));
                            logger.Trace("hazelCastID=" + ac.jSessionId);
                        }
                    }
                }
            }
            return ac;
        }
        private string GenAuthorizationJsonBody(string username, string password, int tenantId)
        {
            string jsonBody;
            if (tenantId == 0)
            {
                jsonBody = @"{
                ""locale"":""en"",
                ""userId"":""USERNAME"",
                ""password"":""PASSWORD"",
                ""language"": ""en""
                  }";
                jsonBody = jsonBody.Replace("USERNAME", username).Replace("PASSWORD", password);
            }
            else
            {
                jsonBody = @"{
                ""locale"":""en"",
                ""userId"":""USERNAME"",
                ""password"":""PASSWORD"",
                ""language"": ""en"",
                ""tenantId"": TENANT_ID
                }";
                jsonBody = jsonBody.Replace("USERNAME", username).Replace("PASSWORD", password).Replace("TENANT_ID", tenantId.ToString());
            }
            return (jsonBody);
        }
    }
}
