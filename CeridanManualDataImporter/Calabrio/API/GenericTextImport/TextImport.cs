﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace CeridianManualDataImporter.Calabrio.API.GenericTextImport
{
    enum TextType { email, chat, twitter, website, other };
    class TextImport
    {
        private static readonly NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        string apiUri = "/api/rest/cas/importtext";
        
        public Boolean submitPost(string calabrioURI, string jsonBody, Authentication.Authorization.AuthenticationCookies ac)
        {
            ServicePointManager.ServerCertificateValidationCallback +=
              (sender, cert, chain, sslPolicyErrors) => true;

            var cookieContainer = new CookieContainer();
            using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })
            {
                using (var client = new HttpClient(handler) { BaseAddress = new Uri(calabrioURI) })
                {
                    client.BaseAddress = new Uri(calabrioURI);
                    client.DefaultRequestHeaders
                        .Accept
                        .Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    // Split cookie into parts
                    string[] hazel = ac.hazelCastSessionId.Split('=');
                    string[] jsession = ac.jSessionId.Split('=');

                    cookieContainer.Add(client.BaseAddress, new Cookie(hazel[0], hazel[1]));
                    cookieContainer.Add(client.BaseAddress, new Cookie(jsession[0], jsession[1]));

                    HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, apiUri);
                    request.Content = new StringContent(jsonBody,
                        Encoding.UTF8,
                        "application/json");

                    logger.Trace("Attempting to post:" + jsonBody);
                    var response = client.SendAsync(request).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        var header = new Helpers.headers();
                        logger.Info(header.getHeaderLogMessage(response.Headers) + " Request succesfully submitted to Calabrio.");
                        return true;
                    }
                    else
                    {
                        var header = new Helpers.headers();
                        logger.Error(header.getHeaderLogMessage(response.Headers));
                        logger.Error("Failed to insert request: " + response.StatusCode + " - " + response.ReasonPhrase);
                        logger.Error(response);
                        logger.Error(response.Content.ReadAsStringAsync().Result);
                        logger.Error("Request:" + jsonBody);
                        return false;
                    }
                }
            }
        }

        /// <summary>
        /// Creates the JSON body required to post to the "/api/rest/cas/importtext" API based on parameters passed in while ensuring that the required paramters are passed in. 
        /// </summary>
        /// <param name="receiver">A list of strings designating the recipients. Delimited by pipes.</param>
        /// <param name="sender">A string designating the sender. This does not need to be an email address.</param>
        /// <param name="textType">Identifies the source of the text. The value should be one of the following: { email, chat, twitter, website, other }</param>
        /// <param name="time">Specifies the start time associated with the contact. The format is UNIX epoch time in milliseconds.</param>
        /// <param name="text">The body of the contact that contains the conversation.</param>
        /// <param name="username">The login of the user associated with the contact.</param>
        /// <param name="id">An identifier from the source associated with the contact. This is not the CCR ID. The contact will not be imported if the ID has already been used for a previous contact.</param>
        /// <param name="metadata">Any metadata associated with the contact. The names must match defined metadata fields. </param>
        /// <param name="evalForm_Id">The evaluation form assigned to contacts if the contact is determined to be marked for quality. You can either provide an ID or name. If an ID is not provided, the API looks up the evaluation form by name.</param>
        /// <param name="evalForm_Name">The evaluation form assigned to contacts if the contact is determined to be marked for quality. You can either provide an ID or name. If an ID is not provided, the API looks up the evaluation form by name.</param>
        /// <param name="subject">The subject associated with the contact.</param>
        /// <param name="references">Typically used in emails to track threaded discussions. Delimited by pipes.</param>
        /// <returns>JSON body as a string value.</returns>
        /// 
        public string assembleJsonBody(
        string[] receiver, string sender, TextType textType, long time, string text, string username,
        string id = null, Dictionary<string, string> metadata = null, int evalForm_Id = 0, string evalForm_Name = null, string subject = null, string[] references = null)
        {
            try
            {
                logger.Trace("Attempting to assemble JSON body");
                JObject json =
                    new JObject(
                        new JProperty("textType", Enum.GetName(typeof(TextType), textType)));
                //Add evaluation Form to Json:
                if (evalForm_Id > 0 && string.IsNullOrEmpty(evalForm_Name) == false)
                {
                    json.Add("evalForm",
                       new JObject(
                         new JProperty("id", evalForm_Id),
                         new JProperty("name", evalForm_Name)));
                }
                if (evalForm_Id > 0 && string.IsNullOrEmpty(evalForm_Name) == true)
                {
                    json.Add("evalForm",
                        new JObject(
                          new JProperty("id", evalForm_Id)));
                }
                if (evalForm_Id == 0 && string.IsNullOrEmpty(evalForm_Name) == false)
                {
                    json.Add("evalForm",
                        new JObject(
                            new JProperty("name", evalForm_Name)));
                }

                JObject recordData = new JObject(
                    new JProperty("time", time),
                    new JProperty("username", username),
                    new JProperty("sender", sender),
                    new JProperty("receiver", new JArray(receiver)),
                    new JProperty("text", text)
                    );

                if (string.IsNullOrEmpty(subject) == false)
                {
                    recordData.Add(new JProperty("subject", subject));
                }

                if (metadata != null)
                {
                    JObject jMetadata = new JObject();
                    foreach (var item in metadata)
                    {
                        jMetadata.Add(new JProperty(item.Key, item.Value));
                    }
                    recordData.Add(new JProperty("metadata", jMetadata));
                }
                if (references != null)
                {
                    recordData.Add(new JProperty("references", references));
                }
                if (string.IsNullOrEmpty(id) == false && Enum.GetName(typeof(TextType), textType) == "email")
                {
                    // This option is only accepted form email text contacts. 
                    recordData.Add(new JProperty("id", id));
                }

                JArray recordArray = new JArray(recordData);

                JProperty records = new JProperty("records", recordArray);

                json.Add(records);
                logger.Trace("JSON assembled");
                return (json.ToString());

            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                logger.Error(ex);
                return null;
            }

        }

        /// <summary>
        /// Returns test text data for testing purposes. 
        /// TODO: Add different tests seperated by switch statements. 
        /// </summary>
        /// <returns></returns>
        public string generateTestData()
        {
            logger.Trace("In JSON assembly test method");
            string[] Recieverlist = { "joshTest@calabrio.com", "test@gmail.com" };
            Dictionary<string, string> metadataDictionary = new Dictionary<string, string>();
            metadataDictionary.Add("CD11", "data1");
            metadataDictionary.Add("CD1", "952-111-1322");
            metadataDictionary.Add("CD7", "josh.majzner@calabrio.com");
            metadataDictionary.Add("CD10", "https://www.calabrio.com");

            string[] referenceArray = { "John", "Cindy", "Joe" };
            var message = "Short Message";
            // logger.Trace(assembleJsonBody(list, "josh.majzner@calabrio.com", TextType.email, "1563558334", "This is a test email.", "josh.majzner@calabrio.com"));
            return (assembleJsonBody(Recieverlist, "josh.majzner@calabrio.com", TextType.email, 1563831690091, message, "josh.majzner@calabrio.com", metadata: metadataDictionary, references: referenceArray, subject: "Email Subject", evalForm_Id: 2));
        }
    } 
}
