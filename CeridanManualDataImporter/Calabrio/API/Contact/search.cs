﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace CeridianManualDataImporter.Calabrio.API.Contact
{
    class search
    {
        private static readonly NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
         Helpers.headers header = new Helpers.headers();

        /// <summary>
        /// Returns a Contact Object
        /// </summary>
        /// <param name="contactID">Calabrio Contact ID // CCR.id (Database)</param>
        /// <returns></returns>
        public contact getContactInfoById(Authentication.Authorization.AuthenticationCookies ac, string baseURL ,string contactID)
        {
            string apiUrl = baseURL + "/api/rest/recording/contact/" + contactID;
            string contents = string.Empty;

            ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;
            var cookieContainer = new CookieContainer();

            using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })
            {
                using (var client = new HttpClient(handler) { BaseAddress = new Uri(baseURL) })
                {
                    client.DefaultRequestHeaders
                        .Accept
                        .Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    // Split cookie into parts
                    string[] hazel = ac.hazelCastSessionId.Split('=');
                    string[] jsession = ac.jSessionId.Split('=');

                    cookieContainer.Add(client.BaseAddress, new Cookie(hazel[0], hazel[1]));
                    cookieContainer.Add(client.BaseAddress, new Cookie(jsession[0], jsession[1]));

                    var result = client.GetAsync(apiUrl).Result;
                    if (result.IsSuccessStatusCode)
                    {
                        logger.Info(header.getHeaderLogMessage(result.Headers) + " Request successfully submitted to Calabrio.");
                        contents = result.Content.ReadAsStringAsync().Result;
                        JObject response = JObject.Parse(contents);
                        contact _contact = new contact(contents);
                        logger.Trace(_contact);
                        return (_contact);
                   
                    }
                    else
                    {
                        logger.Error(header.getHeaderLogMessage(result.Headers) + " Request failed submission to Calabrio.");
                        contents = result.Content.ReadAsStringAsync().Result;
                        logger.Trace(contents);
                        return null;
                    }
                }
            }
        }

        public List<contact> getContactsByAssocCallId(Authentication.Authorization.AuthenticationCookies ac, string baseURL, string associatedCallId)
        {
            List<contact> ContactsFoundWithAssocCallId = new List<contact>();
            string apiUrl = baseURL + "/api/rest/recording/contact?expand=metadata&reason=recorded&searchScope=allEvaluations&range=date_range_in_the_past_year&assocCallId=" + associatedCallId;
            string contents = string.Empty;

            ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;
            var cookieContainer = new CookieContainer();

            using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })
            {
                using (var client = new HttpClient(handler) { BaseAddress = new Uri(baseURL) })
                {
                    client.DefaultRequestHeaders
                        .Accept
                        .Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    // Split cookie into parts
                    string[] hazel = ac.hazelCastSessionId.Split('=');
                    string[] jsession = ac.jSessionId.Split('=');

                    cookieContainer.Add(client.BaseAddress, new Cookie(hazel[0], hazel[1]));
                    cookieContainer.Add(client.BaseAddress, new Cookie(jsession[0], jsession[1]));

                    var result = client.GetAsync(apiUrl).Result;
                    if (result.IsSuccessStatusCode)
                    {
                        logger.Info(header.getHeaderLogMessage(result.Headers) + " Request successfully submitted to Calabrio.");
                        contents = result.Content.ReadAsStringAsync().Result;
                        JArray response = JArray.Parse(contents);

                        foreach (var item in response)
                        {
                            logger.Trace(item.ToString());
                            contact Contact = new contact(item.ToString());
                           // var contact = Contact.FromJson(item.ToString());
                            ContactsFoundWithAssocCallId.Add(Contact);
                        }
                    }
                    else
                    {
                        logger.Error(header.getHeaderLogMessage(result.Headers) + " Request failed submission to Calabrio.");
                        contents = result.Content.ReadAsStringAsync().Result;
                        logger.Trace(contents);
                        //return null;
                    }
                }
            }

            return (ContactsFoundWithAssocCallId);
        }

    }
}

