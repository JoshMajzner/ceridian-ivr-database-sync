﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace CeridianManualDataImporter.Calabrio.API.Contact
{
    class delete
    {
        private static readonly NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private string apiURL = "/api/rest/recording/contact/";

        public bool deleteContact(Authentication.Authorization.AuthenticationCookies ac, string CalabrioURL, string contactID)
        {
            // Search for the contact record to get the JSON body that needs to be modifed for deletion. 
            search search = new search();
            contact Contact = search.getContactInfoById(ac, CalabrioURL, contactID); // Contact ID

            logger.Trace("Attempting to modify contact upload states to recycled.");
            // Modify the Contact Data: 
            Contact.Contact.AudioUploadState = 6;
            Contact.Contact.ScreenUploadState = 6;
            Contact.Contact.Reason.ReasonId = -2;

            logger.Trace("Contact states modifed");
            JObject updatedContact = (JObject)JToken.FromObject(Contact.Contact); // Convert contact to JSON. 

            logger.Info("Attempting to delete Calabrio Contact: " + contactID);          
            
            return (SubmitDelete(CalabrioURL, updatedContact.ToString(), ac));
        }

        public bool SubmitDelete(string calabrioURI, string jsonBody, Authentication.Authorization.AuthenticationCookies ac)
        {
            logger.Trace("Attempting PUT request to Calabrio API");
            ServicePointManager.ServerCertificateValidationCallback +=
              (sender, cert, chain, sslPolicyErrors) => true;

            var cookieContainer = new CookieContainer();
            using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })
            {
                using (var client = new HttpClient(handler) { BaseAddress = new Uri(calabrioURI) })
                {
                    client.BaseAddress = new Uri(calabrioURI);
                    client.DefaultRequestHeaders
                        .Accept
                        .Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    // Split cookie into parts
                    string[] hazel = ac.hazelCastSessionId.Split('=');
                    string[] jsession = ac.jSessionId.Split('=');

                    cookieContainer.Add(client.BaseAddress, new Cookie(hazel[0], hazel[1]));
                    cookieContainer.Add(client.BaseAddress, new Cookie(jsession[0], jsession[1]));

                    HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Put, apiURL);
                    request.Content = new StringContent(jsonBody,
                        Encoding.UTF8,
                        "application/json");

                    logger.Trace("Attempting to post:" + jsonBody);
                    var response = client.SendAsync(request).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        var header = new Helpers.headers();
                        logger.Info(header.getHeaderLogMessage(response.Headers) + " Request succesfully submitted to Calabrio.");
                        return true;
                    }
                    else
                    {
                        var header = new Helpers.headers();
                        logger.Error(header.getHeaderLogMessage(response.Headers));
                        logger.Error("Failed to insert request: " + response.StatusCode + " - " + response.ReasonPhrase);
                        logger.Error(response);
                        logger.Error(response.Content.ReadAsStringAsync().Result);
                        logger.Error("Request:" + jsonBody);
                        return false;
                    }
                }
            }
        }
    }
}
