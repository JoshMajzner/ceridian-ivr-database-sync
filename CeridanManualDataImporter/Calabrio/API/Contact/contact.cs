﻿
namespace CeridianManualDataImporter.Calabrio.API.Contact
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public class contact
    {
        public string originalJson { get; set; }
        public ContactData Contact = new ContactData();

        public contact(string json)
        {
            Contact = ContactData.FromJson(json);
            originalJson = json;
        }

    }

    public partial class ContactData
    {   
        [JsonProperty("id")]
        public object Id { get; set; }

        [JsonProperty("startTime")]
        public object StartTime { get; set; }

        [JsonProperty("tz")]
        public object Tz { get; set; }

        [JsonProperty("tzOffset")]
        public object TzOffset { get; set; }

        [JsonProperty("callDuration")]
        public object CallDuration { get; set; }

        [JsonProperty("ani")]
        [JsonConverter(typeof(ParseStringConverter))]
        public object Ani { get; set; }

        [JsonProperty("dnis")]
        public object Dnis { get; set; }

        [JsonProperty("line")]
        public object Line { get; set; }

        [JsonProperty("audioUploadState")]
        public object AudioUploadState { get; set; }

        [JsonProperty("screenUploadState")]
        public object ScreenUploadState { get; set; }

        [JsonProperty("recordingType")]
        public object RecordingType { get; set; }

        [JsonProperty("icmCallId")]
        [JsonConverter(typeof(ParseStringConverter))]
        public object IcmCallId { get; set; }

        [JsonProperty("assocCallId")]
        public object AssocCallId { get; set; }

        [JsonProperty("contactType")]
        public string ContactType { get; set; }

        [JsonProperty("isCalibration")]
        public object IsCalibration { get; set; }

        [JsonProperty("fromAddress")]
        public object FromAddress { get; set; }

        [JsonProperty("toAddress")]
        public object ToAddress { get; set; }

        [JsonProperty("subject")]
        public object Subject { get; set; }

        [JsonProperty("dailyWorkflowRule")]
        public object DailyWorkflowRule { get; set; }

        [JsonProperty("endOfCallWorkflowRule")]
        public object EndOfCallWorkflowRule { get; set; }

        [JsonProperty("surveyScore")]
        public object SurveyScore { get; set; }

        [JsonProperty("surveyName")]
        public object SurveyName { get; set; }

        [JsonProperty("isRootRecording")]
        public object IsRootRecording { get; set; }

        [JsonProperty("evalStateId")]
        public object EvalStateId { get; set; }

        [JsonProperty("videoFileUploadStateId")]
        public object VideoFileUploadStateId { get; set; }

        [JsonProperty("audioFileUploadStateId")]
        public object AudioFileUploadStateId { get; set; }

        [JsonProperty("isInbound")]
        public object IsInbound { get; set; }

        [JsonProperty("agent")]
        public Agent Agent { get; set; }

        [JsonProperty("team")]
        public Group Team { get; set; }

        [JsonProperty("group")]
        public Group Group { get; set; }

        [JsonProperty("recordingUrl")]
        public object RecordingUrl { get; set; }

        [JsonProperty("training")]
        public object Training { get; set; }

        [JsonProperty("hr")]
        public object Hr { get; set; }

        [JsonProperty("reason")]
        public Reason Reason { get; set; }

        [JsonProperty("evalScorePrediction")]
        public object EvalScorePrediction { get; set; }

        [JsonProperty("netPromoterScore")]
        public object NetPromoterScore { get; set; }

        [JsonProperty("netPromoterScorePrediction")]
        public object NetPromoterScorePrediction { get; set; }

        [JsonProperty("sentiment")]
        public object Sentiment { get; set; }

        [JsonProperty("evalForm")]
        public object EvalForm { get; set; }

        [JsonProperty("evaluator")]
        public Agent Evaluator { get; set; }

        [JsonProperty("approver")]
        public Agent Approver { get; set; }

        [JsonProperty("evaluation")]
        public object Evaluation { get; set; }

        [JsonProperty("goal")]
        public object Goal { get; set; }

        [JsonProperty("events")]
        public Events Events { get; set; }

        [JsonProperty("eventCalculations")]
        public EventCalculations EventCalculations { get; set; }

        [JsonProperty("metadata")]
        public Dictionary<string, Metadatum> Metadata { get; set; }
    }

    public partial class Agent
    {
        [JsonProperty("$ref")]
        public object Ref { get; set; }

        [JsonProperty("displayId")]
        public object DisplayId { get; set; }

        [JsonProperty("agentAcdId")]
        public object AgentAcdId { get; set; }

        [JsonProperty("lastName")]
        public object LastName { get; set; }

        [JsonProperty("firstName")]
        public object FirstName { get; set; }

        [JsonProperty("username")]
        public object Username { get; set; }
    }

    public partial class EventCalculations
    {
        [JsonProperty("talkover")]
        public Hold Talkover { get; set; }

        [JsonProperty("silence")]
        public Hold Silence { get; set; }

        [JsonProperty("pause")]
        public Hold Pause { get; set; }

        [JsonProperty("hold")]
        public Hold Hold { get; set; }
    }

    public partial class Hold
    {
        [JsonProperty("totalDuration")]
        public object TotalDuration { get; set; }

        [JsonProperty("maxDuration")]
        public object MaxDuration { get; set; }

        [JsonProperty("minDuration")]
        public object MinDuration { get; set; }

        [JsonProperty("avgDuration")]
        public object AvgDuration { get; set; }

        [JsonProperty("totalEvents")]
        public object TotalEvents { get; set; }

        [JsonProperty("percentOfCall")]
        public object PercentOfCall { get; set; }
    }

    public partial class Events
    {
        [JsonProperty("$ref")]
        public object Ref { get; set; }
    }

    public partial class Group
    {
        [JsonProperty("$ref")]
        public object Ref { get; set; }

        [JsonProperty("name")]
        public object Name { get; set; }
    }

    public partial class Metadatum
    {
        [JsonProperty("metadata")]
        public object Metadata { get; set; }

        [JsonProperty("name")]
        public object Name { get; set; }

        [JsonProperty("value")]
        public object Value { get; set; }

        [JsonProperty("type")]
        public TypeEnum Type { get; set; }

        [JsonProperty("encrypted")]
        public object Encrypted { get; set; }

        [JsonProperty("exportable")]
        public object Exportable { get; set; }

        [JsonProperty("readOnly")]
        public object ReadOnly { get; set; }
    }
    public enum TypeEnum { Text, HYPERLINK, DATE, NUMBER };

    public partial class Reason
    {
        [JsonProperty("reasonId")]
        public object ReasonId { get; set; }

        [JsonProperty("key")]
        public object Key { get; set; }
    }

    public partial class ContactData
    {
        public static ContactData FromJson(string json) => JsonConvert.DeserializeObject<ContactData>(json, CeridianManualDataImporter.Calabrio.API.Contact.Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this ContactData self) => JsonConvert.SerializeObject(self, CeridianManualDataImporter.Calabrio.API.Contact.Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }

    internal class ParseStringConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(long) || t == typeof(long?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            long l;
            if (Int64.TryParse(value, out l))
            {
                return l;
            }
            throw new Exception("Cannot unmarshal type long");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (long)untypedValue;
            serializer.Serialize(writer, value.ToString());
            return;
        }

        public static readonly ParseStringConverter Singleton = new ParseStringConverter();
    }
}