﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace CeridianManualDataImporter.Calabrio.API.RecordingControls
{
    class Metadata
    {
        private static readonly NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        public bool PostMetadataForContactID(Authentication.Authorization.AuthenticationCookies ac, string calabrioURI, string contactID, Dictionary<string,string> metadataKeyValuePairs)
        {
            logger.Trace("Attempting to post metadata to Calabrio contact: " + contactID);
            string apiURL = "/api/rest/recordingcontrols/metadata?ccrId=" + contactID;
            string jsonBody = generatePostBody(metadataKeyValuePairs);

            if (string.IsNullOrEmpty(jsonBody))
            {
                logger.Error("Metadata Post failed. No Metadata values were sent with the request.");
                return false;
            }
            else
            {
                return (submitPost(ac, calabrioURI, apiURL, jsonBody));
            }         
        }

        private bool submitPost(Authentication.Authorization.AuthenticationCookies ac, string calabrioURI, string apiUri, string jsonBody)
        {
            ServicePointManager.ServerCertificateValidationCallback +=
              (sender, cert, chain, sslPolicyErrors) => true;

            var cookieContainer = new CookieContainer();

            using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })
            {
                using (var client = new HttpClient(handler) { BaseAddress = new Uri(calabrioURI) })
                {
                    client.BaseAddress = new Uri(calabrioURI);
                    client.DefaultRequestHeaders
                        .Accept
                        .Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    // Split cookie into parts
                    string[] hazel = ac.hazelCastSessionId.Split('=');
                    string[] jsession = ac.jSessionId.Split('=');

                    cookieContainer.Add(client.BaseAddress, new Cookie(hazel[0], hazel[1]));
                    cookieContainer.Add(client.BaseAddress, new Cookie(jsession[0], jsession[1]));

                    HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, apiUri);
                    request.Content = new StringContent(jsonBody,
                        Encoding.UTF8,
                        "application/json");
                    logger.Debug("Submitting metadata update request to: " + calabrioURI + apiUri);
                    logger.Trace("Attempting to post:" + jsonBody);
                    var response = client.SendAsync(request).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        var header = new Helpers.headers();
                        logger.Info("Calabrio API Request ID: " + header.getRequestID(response.Headers) + " -- successfully submitted metadata request to Calabrio.");
                        var responseString = response.Content.ReadAsStringAsync();
                        logger.Trace(responseString.Result.ToString());
                        return true;
                    }
                    else
                    {
                        var header = new Helpers.headers();
                        logger.Error("Calabrio API Request ID: " + header.getRequestID(response.Headers) + "HTTP Status: "+ response.StatusCode + " : " + response.ReasonPhrase + " -- failed metadata request to Calabrio." );
                        logger.Error(response.Content.ReadAsStringAsync().Result);
                        logger.Error("Request:" + jsonBody);
                        return false;
                    }              
                }
            }
        }

        private string generatePostBody(Dictionary<string, string> metadataKeyValuePairs)
        {
            logger.Trace("Attempting to generate JSON body with provided metadata values.");
            logger.Trace("Provided metadata entries: ");

            if (metadataKeyValuePairs.Count() > 0 )
            {
                JArray metadata = new JArray();
                foreach (var metadataEntry in metadataKeyValuePairs)
                {
                    logger.Trace("Entry values: '" + metadataEntry.Key + "' : '" + metadataEntry.Value + "'");
                    JObject metadataValue =
                        new JObject(new JProperty("name", metadataEntry.Key),
                        new JProperty("value", metadataEntry.Value));
                    metadata.Add(metadataValue);
                }

                JObject jsonBody = new JObject(new JProperty("metadata", metadata));
                logger.Trace("JSON body: " + jsonBody.ToString());
                logger.Trace("Successfully generated JSON body.");
                return (jsonBody.ToString());
            }
            else
            {
                logger.Error("No metadata values provided.");
                return (null);
            }

        }

    }
}
