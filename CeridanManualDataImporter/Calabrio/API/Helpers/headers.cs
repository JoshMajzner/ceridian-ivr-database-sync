﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;

namespace CeridianManualDataImporter.Calabrio.API.Helpers
{
    class headers
    {
        private static readonly NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        public string getRequestID(HttpResponseHeaders hrh)
        {
            logger.Trace("Attempting to get Request ID from HTTP headers");
            try
            {
                IEnumerable<string> headerValues;
                hrh.TryGetValues("X-Request-ID", out headerValues);
                string requestId = headerValues.FirstOrDefault();
                logger.Trace("Calabrio Request ID found: " + requestId);
                return (requestId);
            }
            catch (Exception ex)
            {
                logger.Error("Calabrio did not return a request ID for this request. " + ex.Message);
                return (null);
            }
        }

        public string getAppServer(HttpResponseHeaders hrh)
        {
            logger.Trace("Attempting to get application server name from HTTP headers");
            try
            {
                IEnumerable<string> headerValues;
                hrh.TryGetValues("x-request-handler-id", out headerValues);
                string appServer = headerValues.FirstOrDefault();
                logger.Trace("Calabrio application server detected: " + appServer);
                return (appServer);
            }
            catch (Exception ex)
            {
                logger.Error("Unable to detect which application server processed this request. " + ex.Message);
                return (null);
            }
        }

        public string getHeaderLogMessage(HttpResponseHeaders hrh)
        {
            return "Calabrio Application Server: " + getAppServer(hrh) + " Calabrio Request ID: " + getRequestID(hrh);

        }

    }

}
