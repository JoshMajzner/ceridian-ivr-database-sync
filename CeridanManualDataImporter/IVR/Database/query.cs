﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CeridianManualDataImporter.IVR.Database
{
    class query
    {
        private static readonly NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private string ConnectionString = @"Data Source=WDC1AVA1AWD02;Initial Catalog=ivr_db_prod;Integrated Security=SSPI;";
        private SqlConnection sqlCon;
       
        private void connectMssql()
        {
            logger.Trace("Attemping to connect to IVR MSSQL Database.");
            sqlCon = new SqlConnection(ConnectionString);
            try
            {
                sqlCon.Open();
                logger.Info("Connected to IVR MSSQL Database.");
            }
            catch (Exception ex)
            {
                logger.Error("Database connection to " + sqlCon.DataSource + " failed to connect.");
                logger.Error(ex.Message);
            }    
        }

        public List<ivrRecord> getIvrData()
        {
            string sqlQuery =
                "DECLARE @startDate datetime; -- Should be 'Yesterday'\r\n" +
                "DECLARE @endDate datetime; -- Should be 'Today'\r\n" +
                "set @startDate = dateadd(day,datediff(day,1,GETDATE()),0);\r\n" +
                "set @endDate = dateadd(day,datediff(day,0,GETDATE()),0);\r\n" +
                "print @startDate \r\n" +
                "print @endDate\r\n" +
                "select * from IVR_DB_PROD.dbo.IVR_CDR_DATA \r\n" +
                "where DATETIME between @startDate and @endDate order by DATETIME asc\r\n";
            var ivrRecords = new List<ivrRecord>();

            try
            {
                connectMssql();
                SqlCommand command = new SqlCommand(sqlQuery, sqlCon);
                SqlDataReader dataReader = command.ExecuteReader();

                while (dataReader.Read())
                {
                    var rowData = new ivrRecord();                           
                    if (dataReader.IsDBNull(0) == false) {rowData.UCID = dataReader.GetString(0);}
                    if (dataReader.IsDBNull(1) == false) { rowData.ORIG_VDN = dataReader.GetString(1); }
                    if (dataReader.IsDBNull(2) == false) { rowData.CLIENT_NAME = dataReader.GetString(2); }
                    if (dataReader.IsDBNull(3) == false) { rowData.FIRST_NAME = dataReader.GetString(3); }
                    if (dataReader.IsDBNull(4) == false) { rowData.LAST_NAME = dataReader.GetString(4); }
                    if (dataReader.IsDBNull(5) == false) { rowData.USERID = dataReader.GetDecimal(5); }
                    if (dataReader.IsDBNull(6) == false) { rowData.DEST_Q = dataReader.GetString(6); }
                    if (dataReader.IsDBNull(7) == false) { rowData.DEST_VMAIL = dataReader.GetString(7); }
                    if (dataReader.IsDBNull(8) == false) { rowData.MSA_ID = dataReader.GetInt32(8); }
                    if (dataReader.IsDBNull(9) == false) { rowData.LANGUAGE = dataReader.GetString(9); }
                    if (dataReader.IsDBNull(10) == false) { rowData.DATETIME = dataReader.GetDateTime(10); }
                    ivrRecords.Add(rowData);                   
                }
                logger.Info("Results found from IVR database: " + ivrRecords.Count());
                return (ivrRecords);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                return (ivrRecords);
            }
            finally
            {        
                CloseMssqlConnection();          
            }
        }


        private void CloseMssqlConnection()
        {
            logger.Trace("Attemping to close connection to IVR MSSQL Database.");
            try
            {
                sqlCon.Close();
                logger.Debug("Disconnected from IVR MSSQL Database.");
            }
            catch (Exception ex)
            {
                logger.Error("Failed to disconnect from " + sqlCon.DataSource);
                logger.Error(ex.Message);

            }
        }


    }
}
