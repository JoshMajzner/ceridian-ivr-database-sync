﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CeridianManualDataImporter.IVR.Database
{
    class ivrRecord
    {
        public string UCID { get; set; }
        public string ORIG_VDN { get; set; }
        public string CLIENT_NAME { get; set; }
        public string FIRST_NAME { get; set; }
        public string LAST_NAME { get; set; }
        public decimal USERID { get; set; }
        public string DEST_Q { get; set; }
        public string DEST_VMAIL { get; set; }
        public int MSA_ID { get; set; }
        public string LANGUAGE { get; set; }
        public DateTime DATETIME { get; set; }
    }
}
